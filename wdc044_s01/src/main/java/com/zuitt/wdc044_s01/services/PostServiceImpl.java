package com.zuitt.wdc044_s01.services;


import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl  implements  PostService{
    @Autowired
    private PostRepository postRepository;

    public void createPost(Post post){
        postRepository.save(post);
    }
    public void updatePost(Long id, Post post){
        Post postForUpdating = postRepository.findById(id).get();
        postForUpdating.setTitle(post
.getTitle());
        postForUpdating.setContent(post.getContent());
        postRepository.save(postForUpdating);
}

public void deletePost(Long id){
        postRepository.deleteById(id);
}
public Iterable<Post> getPosts(){
        return postRepository.findAll();
}
}


